package com.example.illarion.entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "car")
public class car {
	@Id
	@Column(name = "CarID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int CarID;
	@Column(name = "PK")
	int PK;
	@Column(name = "DriverID")
	int	DriverID; 
	@Column(name = "CarManufacturer")
	String CarManufacturer;
	@Column(name = "CarModel")
	String CarModel;
	@Column(name = "CarClass")
	int CarClass;
	@Column(name = "CarSeats")
	int CarSeats;
	public car() {}
	public car(int pK, int driverID, String carManufacturer, String carModel, int carClass, int carSeats) {
		super();
		PK = pK;
		DriverID = driverID;
		CarManufacturer = carManufacturer;
		CarModel = carModel;
		CarClass = carClass;
		CarSeats = carSeats;
	}
	public int getCarID() {
		return CarID;
	}
	public void setCarID(int carID) {
		CarID = carID;
	}
	public int getPK() {
		return PK;
	}
	public void setPK(int pK) {
		PK = pK;
	}
	public int getDriverID() {
		return DriverID;
	}
	public void setDriverID(int driverID) {
		DriverID = driverID;
	}
	public String getCarManufacturer() {
		return CarManufacturer;
	}
	public void setCarManufacturer(String carManufacturer) {
		CarManufacturer = carManufacturer;
	}
	public String getCarModel() {
		return CarModel;
	}
	public void setCarModel(String carModel) {
		CarModel = carModel;
	}
	public int getCarClass() {
		return CarClass;
	}
	public void setCarClass(int carClass) {
		CarClass = carClass;
	}
	public int getCarSeats() {
		return CarSeats;
	}
	public void setCarSeats(int carSeats) {
		CarSeats = carSeats;
	}
	@Override
	public String toString() {
		return "car [CarID=" + CarID + ", PK=" + PK + ", DriverID=" + DriverID + ", CarManufacturer=" + CarManufacturer
				+ ", CarModel=" + CarModel + ", CarClass=" + CarClass + ", CarSeats=" + CarSeats + "]";
	}
	
}
