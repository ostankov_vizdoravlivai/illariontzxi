package com.example.illarion.entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "client")
public class client {
	@Id
	@Column(name = "ClientID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int ClientID; 
	@Column(name = "ClientFIO")
	String ClientFIO; 
	@Column(name = "ClientPhone")
	String ClientPhone;
	@Column(name = "ClientPassword")
	String ClientPassword;
	@Column(name = "ClientRating")
	float ClientRating;
	public client() {}
	public client(String clientFIO, String clientPhone, String clientPassword, float clientRating) {
		super();
		ClientFIO = clientFIO;
		ClientPhone = clientPhone;
		ClientPassword = clientPassword;
		ClientRating = clientRating;
	}
	public int getClientID() {
		return ClientID;
	}
	public void setClientID(int clientID) {
		ClientID = clientID;
	}
	public String getClientFIO() {
		return ClientFIO;
	}
	public void setClientFIO(String clientFIO) {
		ClientFIO = clientFIO;
	}
	public String getClientPhone() {
		return ClientPhone;
	}
	public void setClientPhone(String clientPhone) {
		ClientPhone = clientPhone;
	}
	public String getClientPassword() {
		return ClientPassword;
	}
	public void setClientPassword(String clientPassword) {
		ClientPassword = clientPassword;
	}
	public float getClientRating() {
		return ClientRating;
	}
	public void setClientRating(float clientRating) {
		ClientRating = clientRating;
	}
	
}
