package com.example.illarion.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "administrator")
public class administrator {

	@Id
	@Column(name = "AdminID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int AdminID;
	@Column(name = "AdminFIO")
	String AdminFIO;
	@Column(name = "AdminPhoneNumber")
	String AdminPhoneNumber;
	@Column(name = "AdminPassword")
	String AdminPassword;
	public administrator() {}
	public administrator(String adminFIO, String adminPhoneNumber, String adminPassword) {
		super();
		AdminFIO = adminFIO;
		AdminPhoneNumber = adminPhoneNumber;
		AdminPassword = adminPassword;
	}
	public int getAdminID() {
		return AdminID;
	}
	public void setAdminID(int adminID) {
		AdminID = adminID;
	}
	public String getAdminFIO() {
		return AdminFIO;
	}
	public void setAdminFIO(String adminFIO) {
		AdminFIO = adminFIO;
	}
	public String getAdminPhoneNumber() {
		return AdminPhoneNumber;
	}
	public void setAdminPhoneNumber(String adminPhoneNumber) {
		AdminPhoneNumber = adminPhoneNumber;
	}
	public String getAdminPassword() {
		return AdminPassword;
	}
	public void setAdminPassword(String adminPassword) {
		AdminPassword = adminPassword;
	}
	@Override
	public String toString() {
		return "administrator [AdminID=" + AdminID + ", AdminFIO=" + AdminFIO + ", AdminPhoneNumber=" + AdminPhoneNumber
				+ ", AdminPassword=" + AdminPassword + "]";
	}
	
}
