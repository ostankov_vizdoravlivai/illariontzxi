package com.example.illarion.entities;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "taxiorder")
public class taxiorder {
	@Id
	@Column(name = "OrderID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int OrderID;
	@Column(name = "ClientID")
	int ClientID;
	@Column(name = "DriverID")
	int DriverID;
	@Column(name = "CarID")
	int CarID; 
	@Column(name = "OrderPrice")
	int OrderPrice; 
	@Column(name = "OrderStartTime")
	Timestamp OrderStartTime; 
	@Column(name = "OrderFinishTime")
	Timestamp OrderFinishTime;
	public taxiorder() {}
	public taxiorder(int clientID, int driverID, int carID, int orderPrice, Timestamp orderStartTime,
			Timestamp orderFinishTime) {
		super();
		ClientID = clientID;
		DriverID = driverID;
		CarID = carID;
		OrderPrice = orderPrice;
		OrderStartTime = orderStartTime;
		OrderFinishTime = orderFinishTime;
	}
	public int getOrderID() {
		return OrderID;
	}
	public void setOrderID(int orderID) {
		OrderID = orderID;
	}
	public int getClientID() {
		return ClientID;
	}
	public void setClientID(int clientID) {
		ClientID = clientID;
	}
	public int getDriverID() {
		return DriverID;
	}
	public void setDriverID(int driverID) {
		DriverID = driverID;
	}
	public int getCarID() {
		return CarID;
	}
	public void setCarID(int carID) {
		CarID = carID;
	}
	public int getOrderPrice() {
		return OrderPrice;
	}
	public void setOrderPrice(int orderPrice) {
		OrderPrice = orderPrice;
	}
	public Timestamp getOrderStartTime() {
		return OrderStartTime;
	}
	public void setOrderStartTime(Timestamp orderStartTime) {
		OrderStartTime = orderStartTime;
	}
	public Timestamp getOrderFinishTime() {
		return OrderFinishTime;
	}
	public void setOrderFinishTime(Timestamp orderFinishTime) {
		OrderFinishTime = orderFinishTime;
	}
	
}
