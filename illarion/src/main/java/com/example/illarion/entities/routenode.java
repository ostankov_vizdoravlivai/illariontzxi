package com.example.illarion.entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "routenode")
public class routenode {
	@Id
	@Column(name = "NodeID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int NodeID;
	@Column(name = "OrderID")
	int OrderID; 
	@Column(name = "NodeCoordinates")
	String NodeCoordinates;
	@Column(name = "NodeNumber")
	int NodeNumber;
	@Column(name = "TimeOfStay")
	int TimeOfStay;
	public routenode() {}
	public routenode(int orderID, String nodeCoordinates, int nodeNumber, int timeOfStay) {
		super();
		OrderID = orderID;
		NodeCoordinates = nodeCoordinates;
		NodeNumber = nodeNumber;
		TimeOfStay = timeOfStay;
	}
	public int getNodeID() {
		return NodeID;
	}
	public void setNodeID(int nodeID) {
		NodeID = nodeID;
	}
	public int getOrderID() {
		return OrderID;
	}
	public void setOrderID(int orderID) {
		OrderID = orderID;
	}
	public String getNodeCoordinates() {
		return NodeCoordinates;
	}
	public void setNodeCoordinates(String nodeCoordinates) {
		NodeCoordinates = nodeCoordinates;
	}
	public int getNodeNumber() {
		return NodeNumber;
	}
	public void setNodeNumber(int nodeNumber) {
		NodeNumber = nodeNumber;
	}
	public int getTimeOfStay() {
		return TimeOfStay;
	}
	public void setTimeOfStay(int timeOfStay) {
		TimeOfStay = timeOfStay;
	}
	
}
