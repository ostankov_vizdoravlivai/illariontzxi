package com.example.illarion.entities;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "taximessage")
public class taximessage {
	@Id
	@Column(name = "MessageID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int MessageID;
	@Column(name = "ClientID")
	int ClientID;
	@Column(name = "DriverID")
	int DriverID;
	@Column(name = "OrderID")
	int OrderID;
	@Column(name = "MessageText")
	String MessageText;
	@Column(name = "MessageTypeOfUser")
	int MessageTypeOfUser;
	@Column(name = "MessageDateTime")
	Timestamp MessageDateTime;
	public taximessage() {}
	public taximessage(int clientID, int driverID, int orderID, String messageText, int messageTypeOfUser,
			Timestamp messageDateTime) {
		super();
		ClientID = clientID;
		DriverID = driverID;
		OrderID = orderID;
		MessageText = messageText;
		MessageTypeOfUser = messageTypeOfUser;
		MessageDateTime = messageDateTime;
	}
	public int getMessageID() {
		return MessageID;
	}
	public void setMessageID(int messageID) {
		MessageID = messageID;
	}
	public int getClientID() {
		return ClientID;
	}
	public void setClientID(int clientID) {
		ClientID = clientID;
	}
	public int getDriverID() {
		return DriverID;
	}
	public void setDriverID(int driverID) {
		DriverID = driverID;
	}
	public int getOrderID() {
		return OrderID;
	}
	public void setOrderID(int orderID) {
		OrderID = orderID;
	}
	public String getMessageText() {
		return MessageText;
	}
	public void setMessageText(String messageText) {
		MessageText = messageText;
	}
	public int getMessageTypeOfUser() {
		return MessageTypeOfUser;
	}
	public void setMessageTypeOfUser(int messageTypeOfUser) {
		MessageTypeOfUser = messageTypeOfUser;
	}
	public Timestamp getMessageDateTime() {
		return MessageDateTime;
	}
	public void setMessageDateTime(Timestamp messageDateTime) {
		MessageDateTime = messageDateTime;
	}
	
}
