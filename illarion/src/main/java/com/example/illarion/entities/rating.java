package com.example.illarion.entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rating")
public class rating {
	@Id
	@Column(name = "RatingID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int RatingID;
	@Column(name = "OrderID")
	int OrderID;
	@Column(name = "DriverPoints")
	int DriverPoints; 
	@Column(name = "ClientPoints")
	int ClientPoints; 
	@Column(name = "DriverComment")
	String DriverComment;
	@Column(name = "ClientComment")
	String ClientComment;
	public rating() {}
	public rating(int orderID, int driverPoints, int clientPoints, String driverComment, String clientComment) {
		super();
		OrderID = orderID;
		DriverPoints = driverPoints;
		ClientPoints = clientPoints;
		DriverComment = driverComment;
		ClientComment = clientComment;
	}
	public int getRatingID() {
		return RatingID;
	}
	public void setRatingID(int ratingID) {
		RatingID = ratingID;
	}
	public int getOrderID() {
		return OrderID;
	}
	public void setOrderID(int orderID) {
		OrderID = orderID;
	}
	public int getDriverPoints() {
		return DriverPoints;
	}
	public void setDriverPoints(int driverPoints) {
		DriverPoints = driverPoints;
	}
	public int getClientPoints() {
		return ClientPoints;
	}
	public void setClientPoints(int clientPoints) {
		ClientPoints = clientPoints;
	}
	public String getDriverComment() {
		return DriverComment;
	}
	public void setDriverComment(String driverComment) {
		DriverComment = driverComment;
	}
	public String getClientComment() {
		return ClientComment;
	}
	public void setClientComment(String clientComment) {
		ClientComment = clientComment;
	}
	
}
