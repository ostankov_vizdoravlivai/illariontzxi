package com.example.illarion.entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "driver")
public class driver {
	@Id
	@Column(name = "DriverID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int DriverID;
	@Column(name = "DriverFIO")
	String DriverFIO;
	@Column(name = "DriverPhoneNumber")
	String DriverPhoneNumber;
	@Column(name = "DriverPassword")
	String DriverPassword;
	@Column(name = "DriverRating")
	float DriverRating;
	public driver() {}
	public driver(String driverFIO, String driverPhoneNumber, String driverPassword, float driverRating) {
		super();
		DriverFIO = driverFIO;
		DriverPhoneNumber = driverPhoneNumber;
		DriverPassword = driverPassword;
		DriverRating = driverRating;
	}
	public int getDriverID() {
		return DriverID;
	}
	public void setDriverID(int driverID) {
		DriverID = driverID;
	}
	public String getDriverFIO() {
		return DriverFIO;
	}
	public void setDriverFIO(String driverFIO) {
		DriverFIO = driverFIO;
	}
	public String getDriverPhoneNumber() {
		return DriverPhoneNumber;
	}
	public void setDriverPhoneNumber(String driverPhoneNumber) {
		DriverPhoneNumber = driverPhoneNumber;
	}
	public String getDriverPassword() {
		return DriverPassword;
	}
	public void setDriverPassword(String driverPassword) {
		DriverPassword = driverPassword;
	}
	public float getDriverRating() {
		return DriverRating;
	}
	public void setDriverRating(float driverRating) {
		DriverRating = driverRating;
	}
	
}
