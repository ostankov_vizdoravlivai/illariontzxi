package com.example.illarion.DAO;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.illarion.entities.administrator;

@Repository

public class DAOadministrator {

	public EntityManager em;
	@Autowired
	public DAOadministrator(EntityManager em) {
		this.em = em;
	}
	@Transactional
	public List<administrator> list(){
		Session session = em.unwrap(Session.class);
		
		return session.createQuery("from administrator").getResultList();
		
	}
}
