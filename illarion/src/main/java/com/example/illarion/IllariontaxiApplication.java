package com.example.illarion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IllariontaxiApplication {

	public static void main(String[] args) {
		SpringApplication.run(IllariontaxiApplication.class, args);
	}

}
